/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.QueryTest

class TransformUnionTest
    extends QueryTest
    with SparkSessionTestWrapper
    with DeidTables {

  test("test duplicate results") {
    spark.conf.set("spark.sql.crossJoin.enabled", true)
    val gpuDf = spark.sql("""
        select
          1 person_id
        , 1 note_id
        , 1 offset_begin
        , 10 offset_end
        , 'bob' snippet
        , 'IPP' note_nlp_source_value
                , '' term_temporal

        , 'neroner' nlp_system
        UNION ALL
           select
          1 person_id
        , 1 note_id
        , 11 offset_begin
        , 20 offset_end
        , 'bob' snippet
        , 'IPP' note_nlp_source_value
                , '' term_temporal

        , 'neroner' nlp_system
        UNION ALL
           select
          1 person_id
        , 1 note_id
        , 11 offset_begin
        , 20 offset_end
            		, 'bob' snippet
        , 'IPP' note_nlp_source_value
                , '' term_temporal

        , 'neroner' nlp_system
        """)

    val sparkDf = spark.sql("""
        select
          1 person_id
        , 1 note_id
        , 1 offset_begin
        , 10 offset_end
            		, 'bob' snippet
        , 'IPP' note_nlp_source_value
                , '' term_temporal

        , 'uima' nlp_system
        """)

    val goldDf = spark.sql("""
        select
          1 person_id
        , 1 note_id
        , 1 offset_begin
        , 10 offset_end
            		, 'bob' snippet
        , 'IPP' note_nlp_source_value
                , '' term_temporal

        , 'uima' nlp_system
        UNION ALL
        select
          1 person_id
        , 1 note_id
        , 11 offset_begin
        , 20 offset_end
    		, 'bob' snippet
        , 'IPP' note_nlp_source_value
        , '' term_temporal
        , 'neroner' nlp_system
        """)

    val dfResult = TransformUnion.removeDuplicate(sparkDf, gpuDf)
    val r =
      DFTool.applySchema(goldDf, schemaNoteNlp).cache.orderBy("nlp_system")
    val v =
      DFTool.applySchema(dfResult, schemaNoteNlp).cache.orderBy("nlp_system")
    //    r.show
    //    v.show
    checkAnswer(r, v)

  }

}
