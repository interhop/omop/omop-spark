/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import org.apache.spark.sql.QueryTest

class ExtractTest extends QueryTest with SparkSessionTestWrapper {

  test("test knowledge") {

    import spark.implicits._
    val observation = ((1L, 4086449, "Smith", 44810199, "NOM") ::
      (1L, 4086933, "jim", 44810199, "PRENOM") ::
      (1L, 4314148, "jim@Smith@riseup.net", 44810199, "MAIL") ::
      (1L, 4083592, "01 01 01 01 01", 44810199, "TEL") ::
      (1L, 4245003, "09090901212091209", 44810199, "SECU") ::
      Nil)
      .toDF(
        "person_id",
        "observation_concept_id",
        "value_as_string",
        "observation_type_concept_id",
        "observation_source_value"
      )

    val note = ((4L, 1L, "note content", 1234) :: Nil)
      .toDF("note_id", "person_id", "note_text", "note_hash")

    val location = ((
      2,
      "48 rue albert",
      "52 bvrd picpus",
      "en face monoprix",
      "Paris",
      "75011"
    ) ::
      Nil).toDF(
      "location_id",
      "address",
      "address_1",
      "address_2",
      "city",
      "zip"
    )

    val locationHistory =
      ((2, "Person", 1L) :: Nil).toDF("location_id", "domain_id", "entity_id")

    val observations = Extract.getObservation(
      spark,
      observation,
      note,
      location,
      locationHistory
    )
    val json = Extract.toJson(observations)
    json.show(false)
  }
}
