/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import com.amazon.deequ.VerificationSuite
import com.amazon.deequ.checks.{Check, CheckLevel, CheckStatus}
import com.amazon.deequ.constraints.ConstraintStatus
import org.apache.spark.sql.SparkSession

object QualityNoteNlp extends App with DeidIO {

  val mode = args(0)
  val database = args(1)
  val inputNoteNlp = args(2)

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Assign")
    .enableHiveSupport()
    .getOrCreate()

  val data = readDataframe(spark, mode, database, inputNoteNlp)

  // see: https://github.com/awslabs/deequ/blob/master/src/main/scala/com/amazon/deequ/checks/Check.scala
  val verificationResult = VerificationSuite()
    .onData(data)
    .addCheck(
      Check(CheckLevel.Error, "unit testing my data")
        .isComplete("note_id") // should never be NULL
        .isComplete("person_id") // should not contain duplicates
        .isComplete("offset_begin") // should not contain duplicates
        .isComplete("offset_end") // should not contain duplicates
        .isComplete("note_nlp_source_value") // should not contain duplicates
        .isComplete("nlp_system") // should not contain duplicates
        .isPrimaryKey("note_id", "person_id", "offset_begin")
        .isComplete("lexical_variant")
        .where("snippet is not null")
    )
    .run()

  if (verificationResult.status == CheckStatus.Success)
    println("The data passed the test, everything is fine!")
  else {
    println("We found errors in the data:\n")

    val resultsForAllConstraints = verificationResult.checkResults
      .flatMap { case (_, checkResult) => checkResult.constraintResults }

    resultsForAllConstraints
      .filter {
        _.status != ConstraintStatus.Success
      }
      .foreach { result =>
        println(s"${result.constraint}: ${result.message.get}")
      }

  }
}
