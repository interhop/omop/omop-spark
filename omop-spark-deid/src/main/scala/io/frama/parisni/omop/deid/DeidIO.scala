/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.omop.deid

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.SaveMode.Overwrite
import java.io.File

trait DeidIO extends LazyLogging with DeidTables {

  def readDataframe(
      spark: SparkSession,
      mode: String,
      path: String,
      file: String
  ): DataFrame = {
    logger.info("Reading %s %s with %s mode".format(path, file, mode))

    mode match {
      case "hive"    => spark.read.table(path + "." + file)
      case "csv"     => spark.read.format("csv").load(path + "/" + file)
      case "delta"   => spark.read.format("delta").load(path + "/" + file)
      case "parquet" => spark.read.format("parquet").load(path + "/" + file)
      case "orc"     => spark.read.format("orc").load(path + "/" + file)
      case _         => throw new Exception("Only csv and hive are implemented")
    }
  }

  def writeDataframe(
      dataFrame: DataFrame,
      mode: String,
      path: String,
      file: String
  ): Unit = {
    logger.info("Writing %s %s with %s mode".format(path, file, mode))

    mode match {
      case "hive" =>
        dataFrame.write
          .format("orc")
          .mode(Overwrite)
          .saveAsTable(path + "." + file)
      case "csv" =>
        dataFrame.write.format("csv").mode(Overwrite).save(path + "/" + file)
      case "delta" =>
        dataFrame.write
          .format("delta")
          .option("mergeSchema", true)
          .mode(Overwrite)
          .save(path + "/" + file)
      case "parquet" =>
        dataFrame.write
          .format("parquet")
          .mode(Overwrite)
          .save(path + "/" + file)
      case "orc" =>
        dataFrame.write.format("orc").mode(Overwrite).save(path + "/" + file)
      case _ => throw new Exception("Only csv and hive are implemented")
    }

  }

  def writeTempDataframe(dataFrame: DataFrame, file: String): Unit = {
    val filePath = TMP_FOLDER + file
    logger.info("Writing %s".format(filePath))

    dataFrame.write
      .mode(Overwrite)
      .format("parquet")
      .save(filePath)

  }

  def readTempDataframe(spark: SparkSession, file: String): DataFrame = {
    val filePath = TMP_FOLDER + file
    logger.info("Reading %s".format(file))

    spark.read.format("parquet").load(filePath)

  }

  protected def getListOfFolder(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).toList
    } else {
      List[File]()
    }
  }

  /**
    * Create in the <path> folder a <number> of empty folders.
    * If the folder already exists, it is replaced with an empty one.
    *
    * @param path
    * @param pattern
    * @param number
    */
  def generateFolders(path: String, pattern: String, number: Int) = {
    if (path.startsWith("/"))
      throw new Exception("folder_gpu SHALL be a relative path only !")

    (0 to (number - 1)).foreach(x =>
      createOrReplaceFolder(path + File.separator + pattern + x)
    )

  }

  protected def createOrReplaceFolder =
    (folder: String) => {
      val file = new File(folder)
      if (file.exists()) {
        file.delete()
        logger.info("Deleting %s".format(folder))
      }
      file.mkdirs()
      logger.info("Creating %s".format(folder))
    }

}
