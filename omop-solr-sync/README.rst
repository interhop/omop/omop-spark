OMOP-SOLR-SYNC
==============

This code aims to build a SOLR index to be used to efficiently search over OMOP.
Right now it focuses on FHIR model and MAPPER, and also COHORT 360 tools.

In particular, dedicated FHIR indexes are transformed and loaded to SOLR from
Postgres thought spark to deliver fast and paginated results.

