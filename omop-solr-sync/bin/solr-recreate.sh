collection=encounterAphp SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=patientAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=conditionAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=procedureAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=compositionAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=documentReferenceAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
collection=claimAphp  SOLR_SHARD=$SOLR_SHARD_LARGE make solr-delete solr-create
###
collection=practitionerAphp  SOLR_SHARD=$SOLR_SHARD_SMALL make solr-delete solr-create
collection=practitionerRoleAphp  SOLR_SHARD=$SOLR_SHARD_SMALL make solr-delete solr-create
collection=organizationAphp  SOLR_SHARD=$SOLR_SHARD_SMALL make solr-delete solr-create
collection=groupAphp  SOLR_SHARD=$SOLR_SHARD_SMALL make solr-delete solr-create
