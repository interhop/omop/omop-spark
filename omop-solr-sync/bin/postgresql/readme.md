# How to store patient personal informations  ?

In the observation table with SNOMED codes 

4086933	Patient surname
4086449	Patient forename
4307270	Maiden name
4083586 Patient address
4314148 Patient - email address
4245003	Identification number
--4083587	Date of birth

44791058 Patient previous surname
4335813	City of residence
4083592	Patient telephone number
4083591	Patient postal code
4160017 Street address
4177383	Patient mobile telephone number
4014292    Place of birth

4053609 Marital status 
	4338692	Married
	4027529	Separated

