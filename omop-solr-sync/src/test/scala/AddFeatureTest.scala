/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.solr

import java.sql.Timestamp

import org.apache.spark.sql.catalyst.ScalaReflection.Schema
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, QueryTest}

class AddFeatureTest
    extends QueryTest
    with SparkSessionTestWrapper
    with OmopTransform
    with T {

  test("test add feature patient") {

    import spark.implicits._
    val observation = ((1L, 2L, new Timestamp(1L), new Timestamp(1L)) :: Nil)
      .toDF("patient", "id", "thing", "_lastUpdated")

    val patient = ((
      1L,
      "male",
      Array("a", "b"),
      new Timestamp(1L),
      new Timestamp(1L),
      true,
      Array("c", "d"),
      new Timestamp(1L),
      Array(1L, 2L)
    ) :: Nil)
      .toDF(
        "patient",
        "gender-simple",
        "identifier",
        "birthdate",
        "death-date",
        "deceased",
        "gender",
        "_lastUpdated",
        "_list"
      )

    val expectedSchema = schemaUnion(patientFeatures, observation.schema)

    val extendedPatient: DataFrame =
      addFeaturesPatient(spark, observation, patient)
    checkSchema(extendedPatient.schema, expectedSchema)

  }

  test("test add feature encounter") {

    import spark.implicits._
    val observation =
      ((1L, 1L, 2L, new Timestamp(1L), new Timestamp(1L)) :: Nil)
        .toDF("patient", "encounter", "id", "thing", "_lastUpdated")

    val encounter = ((
      1L,
      Array("a", "b"),
      new Timestamp(1L),
      new Timestamp(1L),
      true,
      Array("c", "d"),
      "male",
      1L,
      Array("a", "b"),
      1.2d,
      new Timestamp(1L),
      new Timestamp(1L),
      new Timestamp(1L),
      "icu ward",
      Array(1L, 2L)
    ) :: Nil)
      .toDF(
        "patient.patient",
        "patient.identifier",
        "patient.birthdate",
        "patient.death-date",
        "patient.deceased",
        "patient.gender",
        "patient.gender-simple",
        "encounter",
        "identifier",
        "length",
        "start-date",
        "end-date",
        "_lastUpdated",
        "service-provider.display",
        "_list"
      )

    val expectedSchema =
      schemaUnion(patientFeatures, encounterFeatures, observation.schema)

    val extendedEncounter: DataFrame =
      addFeaturesEncounter(spark, observation, encounter)
    checkSchema(extendedEncounter.schema, expectedSchema)

  }

  def checkSchema(s1: StructType, s2: StructType): Boolean = {
    s1.fields.forall(structfield => {
      s2.fields.map(s => s.name).contains(structfield.name)
    })

  }
}
