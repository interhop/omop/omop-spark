/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.solr

import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions.{col, greatest}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

trait OmopTransform extends LazyLogging {

  def addFeaturesEncounter(
      spark: SparkSession,
      initialDf: DataFrame,
      sourceDf: DataFrame
  ): DataFrame = {
    logger.warn("prepending encounter features")
    val prependedDf = prependColumnEncounter(sourceDf)
    val schem =
      schemaUnion(patientFeatures, encounterFeatures, initialDf.schema)
    val result = initialDf
      .as("d")
      .join(
        prependedDf.as("j"),
        col("d.`encounter`") === col("j.`encounter.encounter`"),
        "left"
      )
      .withColumn(
        "_lastUpdated",
        greatest(
          col("d._lastUpdated"),
          col("j.`%s.%s`".format("encounter", "_lastUpdated"))
        )
      )
    DFTool.applySchemaSoft(result, schem)
  }

  def addFeaturesPatient(
      spark: SparkSession,
      initialDf: DataFrame,
      sourceDf: DataFrame
  ): DataFrame = {
    logger.warn("prepending patient features")
    val prependedDf = prependColumnPatient(sourceDf)
    val schem = schemaUnion(patientFeatures, initialDf.schema)
    val result = initialDf
      .as("d")
      .join(
        prependedDf.as("j"),
        col("d.`patient`") === col("j.`patient.patient`"),
        "left"
      )
      .withColumn(
        "_lastUpdated",
        greatest(
          col("d._lastUpdated"),
          col("j.`%s.%s`".format("patient", "_lastUpdated"))
        )
      )
    DFTool.applySchemaSoft(result, schem)
  }

  def schemaUnion(schemas: StructType*) = {
    StructType(schemas.reduce((x, y) => StructType(x.toList ++ y.toList)))
  }

  def prependColumnPatient(df: DataFrame) = {
    var tmp = df
    tmp.columns.foreach { column =>
      if (column != "_list")
        tmp = tmp.withColumnRenamed(column, "patient." + column)
    }
    tmp
  }

  def prependColumnEncounter(df: DataFrame) = {
    var tmp = df
    tmp.columns.foreach { column =>
      if (!column.startsWith("patient.") && column != "_list")
        tmp = tmp.withColumnRenamed(column, "encounter." + column)
    }
    tmp
  }

  val patientFeatures = StructType(
    StructField("patient.identifier", ArrayType(StringType)) ::
      StructField("patient.birthdate", TimestampType) ::
      StructField("patient.death-date", TimestampType) ::
      StructField("patient.deceased", BooleanType) ::
      StructField("patient.gender", ArrayType(StringType)) ::
      StructField("patient.gender-simple", StringType) ::
      StructField("_list", ArrayType(LongType)) ::
      Nil
  )

  val encounterFeatures = StructType(
    StructField("encounter.identifier", ArrayType(StringType), true) ::
      StructField("encounter.length", DoubleType) ::
      StructField("encounter.start-date", TimestampType, true) ::
      StructField("encounter.end-date", TimestampType, true) ::
      StructField("encounter.service-provider.display", StringType, true) ::
      Nil
  )
}
