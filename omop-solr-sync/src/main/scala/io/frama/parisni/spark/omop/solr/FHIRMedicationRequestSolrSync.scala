/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql._

class FHIRMedicationRequestSolrSync(
    spark: SparkSession,
    options: Map[String, String],
    url: String,
    deltaPath: String
) extends SolrSync(spark, options, url, deltaPath)
    with T {

  _collection = "medicationRequestAphp"

  override def transformDelta(conceptFhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    var resource = spark.read.format("delta").load(deltaPath + "measurement")
    this.dfTransformed = FHIRObservationSolrSync.transform(
      resource.repartition(_partitionNumLarge),
      conceptFhir
    )
    this.dfTransformed = DFTool.applySchemaSoft(
      this.dfTransformed,
      FHIRObservationSolrSync.outputSchema
    )

    writeCollection(joinStr = "encounterAphp")

    this

  }

}

object FHIRMedicationRequestSolrSync extends T {

  val outputSchema = types.StructType(
    StructField("id", StringType, false)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("status", ArrayType(StringType), true)
      :: StructField("status-simple", StringType, true)
      :: StructField("diagnosis", StringType, true)
      :: StructField("created", TimestampType, false)
      :: StructField("_text_", StringType, false)
      :: Nil
  )

  def apply(
      spark: SparkSession,
      options: Map[String, String],
      url: String,
      deltaPath: String
  ): FHIRObservationSolrSync = {
    val a = new FHIRObservationSolrSync(spark, options, url, deltaPath)
    return a
  }

  def transform(facts: Dataset[Row], concepts: Dataset[Row]): DataFrame = {

    val resultDf = DFTool.applySchemaSoft(facts, observationSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concepts, conceptFhirSchema)

    val ret = getLastUpdatedDf(
      "f" :: "c" :: "cs" :: Nil,
      resultDf
        .alias("f")
        .withColumnRenamed("measurement_id", "id")
        .withColumnRenamed("incurred_datetime", "created")
        .withColumn("patient", col("person_id"))
        .withColumn("patient-type", lit("Patient"))
        .withColumn("encounter", col("cost_event_id"))
        .withColumn("encounter-type", lit("Encounter"))
        .join(
          (conceptFhirDf).alias("c"),
          col("f.drg_source_concept_id") === col("c.concept_id"),
          "left"
        )
        .join(
          (conceptFhirDf).alias("cs"),
          col("f.row_status_source_concept_id") === col("cs.concept_id"),
          "left"
        )
        .withColumn("_text_", fhirNarrative("c", "cs"))
        .withColumn("diagnosis", col("c.concept_code"))
        .withColumn("status", fhirTokenArray("cs"))
        .withColumn("status-simple", fhirTokenSimple("cs"))
    )
      .select(
        "id",
        "patient",
        "patient-type",
        "encounter",
        "diagnosis",
        "encounter-type",
        "_lastUpdated",
        "status",
        "status-simple",
        "created",
        "_text_"
      )

    return DFTool.applySchemaSoft(ret, outputSchema)
  }
}
