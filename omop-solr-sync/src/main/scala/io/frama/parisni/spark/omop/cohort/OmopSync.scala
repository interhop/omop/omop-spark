/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.cohort

import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.omop.solr._
import io.frama.parisni.spark.postgres.PGTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel

object OmopSync extends App with LazyLogging with OmopStructure {

  lazy val noteCalc = spark.read.format("delta").load(NOTE_CALC)
  val ZK = args(0)
  val PG_COHORT = args(1)
  val DELTA_PATH = args(2)
  val TYPE = args(3)
  val LOG = args(4)
  val NOTE_CALC = args(5) // this contains note feature to be added
  val deltaToFhir = TYPE.toLowerCase().contains("fhir")
  val fhirToSolr = TYPE.toLowerCase().contains("solr")
  val options = Map(
    "zkhost" -> f"$ZK",
    "commit_within" -> "10000",
    "batch_size" -> "5000",
    "timezone_id" -> "Europe/Paris"
  )
  val urlCohort = f"${PG_COHORT}"
  val deltaPath = f"${DELTA_PATH}"

  logger.info("url zk: %s".format(options))
  logger.info("url cohort: %s".format(urlCohort))
  logger.info("url delta: %s".format(deltaPath))
  logger.info("type: %s".format(TYPE))
  logger.info("log: %s".format(LOG))
  logger.info("note calc: %s".format(NOTE_CALC))

  val spark = SparkSession
    .builder()
    .appName("Cohort 360 Sync")
    .getOrCreate()

  spark.sparkContext.setLogLevel(LOG)

  val pg = PGTool(spark, urlCohort, "spark-postgres-cohort")

  try {

    if (deltaToFhir)
      updateFhirFromDelta()

    if (fhirToSolr)
      updateSolrFromFhir()

  } finally {
    pg.purgeTmp()
  }

  System.exit(0)

  def getCohort(): DataFrame = {

    val person = spark.read
      .format("delta")
      .load(deltaPath + "person")
      .select("person_id")
      .alias("person")
    val cohort =
      spark.read.format("delta").load(deltaPath + "cohort").alias("cohort")
    val cohortList = person
      .join(
        cohort,
        col("person.person_id") === col("cohort.subject_id"),
        "left"
      )
      .groupBy("person_id")
      .agg(
        collect_list("cohort_definition_id").alias("_list"),
        max(col("change_datetime")).as("change_datetime")
      )
      .select("person_id", "_list", "change_datetime")

    cohortList

  }

  def getConceptFhir(): DataFrame = {

    spark.read
      .format("delta")
      .load(deltaPath + "concept_fhir")
      .filter(col("concept_id").>=(lit(2000000000L)))
      .select(
        "concept_id",
        "change_datetime",
        "concept_code",
        "concept_name",
        "vocabulary_reference",
        "fhir_concept_code",
        "fhir_concept_name",
        "fhir_vocabulary_reference"
      )
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

  }

  def updateFhirFromDelta() = {

    logger.info("Updating Solr")

    val retConceptFhir = broadcast(getConceptFhir())

    getCohort().write
      .format("parquet")
      .mode(SaveMode.Overwrite)
      .save(deltaPath + "/cohort_list")

    // patient related
    FHIRPatientSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    )
    FHIREncounterSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    ) //TODO: generate both patient/encounter features
    FHIRObservationSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir)
    FHIRConditionSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    )
    FHIRProcedureSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    )
    FHIRDocumentReferenceSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir)
    FHIRClaimSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    )
    FHIRCompositionSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir, noteCalc)

    // not patient related
    FHIRValueSetSolrSync(spark, options, urlCohort, deltaPath).transformDelta(
      retConceptFhir
    )
    FHIROrganizationSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir)
    FHIRPractitionerSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir)
    FHIRPractitionerRoleSolrSync(spark, options, urlCohort, deltaPath)
      .transformDelta(retConceptFhir)

  }

  def updateSolrFromFhir() = {

    logger.info("Updating Solr")

    val cohortDf = broadcast(getCohort())

    // patient related
    FHIRPatientSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIREncounterSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRObservationSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRConditionSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRProcedureSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRDocumentReferenceSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRClaimSolrSync(spark, options, urlCohort, deltaPath).sync(
      cohortDf = Some(cohortDf)
    )
    FHIRCompositionSolrSync(spark, options, urlCohort, deltaPath)
      .sync(numPartitions = Some(1000), cohortDf = Some(cohortDf))

    // not patient related
    FHIRValueSetSolrSync(spark, options, urlCohort, deltaPath).sync()
    FHIROrganizationSolrSync(spark, options, urlCohort, deltaPath).sync()
    FHIRPractitionerSolrSync(spark, options, urlCohort, deltaPath).sync()
    FHIRPractitionerRoleSolrSync(spark, options, urlCohort, deltaPath).sync()

  }
}
