/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.cohort

trait OmopStructure {

  val OMOP_TABLES =
    ("row_id", "note", None) ::
      ("row_id", "cost", "claimAphp") ::
      ("row_id", "care_site", "organizationAphp") ::
      ("row_id", "care_site_history", None) ::
      ("row_id", "concept", None) ::
      ("row_id", "vocabulary", None) ::
      ("row_id", "concept_relationship", None) ::
      ("row_id", "condition_occurrence", "conditionAphp") ::
      ("row_id", "fact_relationship", None) ::
      ("row_id", "location", None) ::
      ("row_id", "location_history", None) ::
      ("row_id", "observation", None) ::
      ("row_id", "person", "patientAphp") ::
      ("row_id", "procedure_occurrence", "procedureAphp") ::
      ("row_id", "provider", "practitionerAphp") ::
      ("row_id", "visit_detail", None) ::
      ("row_id", "visit_occurrence", None) ::
      ("row_id", "concept_fhir", None) ::
      ("row_id", "cohort", None) ::
      ("row_id", "cohort_definition", "groupAphp") ::
      Nil

}
