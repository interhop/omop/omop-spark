/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.sentence

import com.databricks.spark.corenlp.CoreNlpFunctions.ssplit
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object ExtractSentenceFrenchToTable extends App with LazyLogging {

  val indb = args(0)
  val input_table = args(1) // note_id | note_text
  val output_file = args(2) // note_id | note_text

  logger.warn("Source: %s.%s".format(indb, input_table))

  val spark = SparkSession
    .builder()
    .appName("OMOP Sentence Extract")
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  val note_input =
    spark.read.format("delta").load("%s/%s".format(indb, input_table))

  val note_output = note_input
    .select(
      'note_id,
      explode(ssplit(coalesce('note_text, lit("")))) as "sentence"
    )
    .dropDuplicates("sentence")

  toTable(note_output, output_file)
  // save the resulting section table

  def toTable(df: DataFrame, outputTable: String): Unit = {
    df.write
      .format("parquet")
      .mode(SaveMode.Overwrite)
      .save(outputTable)

  }

}
