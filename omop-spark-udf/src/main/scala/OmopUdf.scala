/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.omop.udf

import org.apache.spark.sql.api.java.UDF2

class NumCover extends UDF2[Seq[Seq[Long]], Int, Int] {
  def call(xs: Seq[Seq[Long]], delay_hour: Int): Int = {
    NumCover.calc(xs, delay_hour)
  }
}

object NumCover {

  def calc(xs: Seq[Seq[Long]], delay_hour: Int): Int = {
    val good: Seq[(Long, Long)] = xs.toSeq
      .map(f => { val c = f.toArray; (c(0), c(1)) })
      .filter(f => !(f._2 == 0 && f._1 == 0))
      .sortWith(_._1 < _._1)

    if (good.size <= 1) 0
    else {
      val delay_millis = delay_hour * 86400
      (good, good.drop(1)).zipped
        .map((curr, next) => {
          if (
            curr._1 >= next._1 + delay_millis || curr._1 >= next._2 + delay_millis || curr._2 >= next._1 + delay_millis || curr._2 >= next._2 + delay_millis
          ) 1
          else 0
        })
        .reduce((i: Int, j: Int) => i + j)
    }
  }
}
