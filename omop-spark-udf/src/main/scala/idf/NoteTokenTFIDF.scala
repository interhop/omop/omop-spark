/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.idf

import com.databricks.spark.corenlp.CoreNlpFunctions.{ssplit, tokenize}
import org.apache.spark.sql.api.java.UDF2
import org.apache.spark.sql.functions._
import org.apache.spark.sql._

object NoteTokenTFIDF extends App {

  val indb = args(0)
  val input_table = args(1) // 'note_id, 'note_text
  val outdb = args(2)
  val output_table = args(3) // note_id | sentence | words | postag

  val spark = SparkSession
    .builder()
    .appName("OMOP Section Extract")
    .enableHiveSupport()
    .getOrCreate()

  // get the section file
  // label_section is the exact content of the section
  // cd_section is the concept_code of the section
  // cd_doc is the concept_code of the document in which the section might exist
  val note = spark
    .table("%s.%s".format(indb, input_table))
    .selectExpr("note_event_id as note_id", "note_text")

  new TfIDF(spark)
    .train(note)
    .save("%s.%s".format(outdb, output_table))

  //.fit(note, .65, .7, .7)
  //.write.format("parquet")
  //.mode(org.apache.spark.sql.SaveMode.Overwrite)
  //.saveAsTable("%s.%s".format(outdb, output_table))

}

object NoteTokenTFIDFInfer extends App {

  val indb = args(0)
  val input_table = args(1) // 'note_id, 'note_text
  val outdb = args(2)
  val output_table = args(3) // note_id | sentence | words | postag
  val modeltfidf = args(4)

  val spark = SparkSession
    .builder()
    .appName("Infer Note Token")
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  // get the section file
  // label_section is the exact content of the section
  // cd_section is the concept_code of the section
  // cd_doc is the concept_code of the document in which the section might exist
  val note = spark
    .table("%s.%s".format(indb, input_table))
    .select('note_id, 'note_text)

  new TfIDF(spark)
    .load(s"$modeltfidf")
    .filterModel(
      !(regexp_replace(
        'token,
        "_",
        "-"
      ) rlike "\\b(nom|pr.nom|zip|date|ipp|m.decin|s.curit.|finess|nip|h.pital|social|age|service|aphp|ap-hp|h.pitaux|pr|dr|chef|docteur|ville|header|publique|cedex|footer|ans|secu)\\b")
        && !('token rlike "\\w\\.")
    )
    .fit(note, 5, 3, 2)
    .write
    .format("parquet")
    .mode(org.apache.spark.sql.SaveMode.Overwrite)
    .saveAsTable("%s.%s".format(outdb, output_table))
}

class BiGramUdf extends UDF2[Seq[String], Int, Seq[String]] {
  def call(words: Seq[String], n: Int): Seq[String] = {
    BiGramUdf.call(words, n)
  }
}

object BiGramUdf {

  def call(words: Seq[String], n: Int): Seq[String] = {

    if (words.size >= n)
      words.sliding(n).map(_.mkString("-")).toSeq
    else Nil.toSeq

  }
}

class TfIDF(spark: SparkSession) extends Serializable with Dfcache {

  import spark.implicits._

  private var model: DataFrame = _
  private val stop = broadcast(
    spark.read
      .option("header", true)
      .option("delimiter", "\\t")
      .csv("dicoFinal.csv")
      .filter('type rlike "ART|VER|PRO|PRE|CON|ADJ:pos|ADJ:num")
      .select('mot)
      .dropDuplicates("mot")
  )

  def train(df: DataFrame): this.type = {
    val docCount = df.count()
    model = processTfIdf(process(df), docCount)
    this
  }

  def filterModel(filter: Column) = {
    this.model = model.filter(filter)
    this
  }

  def save(modelPath: String): this.type = {
    model.write
      .mode(SaveMode.Overwrite)
      .format("parquet")
      .saveAsTable(modelPath)
    load(modelPath) // avoid rebuilding the model
    this
  }

  def load(modelPath: String): this.type = {
    model = spark.table(modelPath)
    this
  }

  def fit(
      df: DataFrame,
      cutoffOne: Double,
      cutoffTwo: Double,
      cutoffThree: Double,
      numOne: Int,
      numTwo: Int,
      numThree: Int
  ): DataFrame = {
    import df.sparkSession.implicits._
    val expdtokens = process(df)

    import org.apache.spark.sql.expressions.Window
    val wind = Window.partitionBy('note_id, 'ngram).orderBy('tf_idf.desc)
    val rk = row_number().over(wind).as("rank")
    val filteredTfIdf = processNgram(expdtokens)
      .filter(expr(s"""
          (tf_idf >= $cutoffOne AND ngram = 1)
          OR
          (tf_idf >= $cutoffTwo AND ngram = 2)
          OR
          (tf_idf >= $cutoffThree AND ngram = 3)
          """))
      .withColumn("rank", rk)

    val ranked = filteredTfIdf.filter(expr(s"""
          (rank <= $numOne AND ngram = 1)
          OR
          (rank <= $numTwo AND ngram = 2)
          OR
          (rank <= $numThree AND ngram = 3)
           """))

    ranked.groupBy('note_id).agg(collect_list('token) as "token_idf")
  }

  def fit(
      df: DataFrame,
      cutoffOne: Double,
      cutoffTwo: Double,
      cutoffThree: Double
  ): DataFrame = {
    import df.sparkSession.implicits._
    val expdtokens = process(df)
    val tfidf = processNgram(expdtokens)

    val filteredTfIdf = tfidf.filter(
      ('ngram === lit(1) && 'tf_idf.>=(cutoffOne))
        || ('ngram === lit(2) && 'tf_idf.>=(cutoffTwo))
        || ('ngram === lit(3) && 'tf_idf.>=(cutoffThree))
    )

    filteredTfIdf.groupBy('note_id).agg(collect_list('token) as "token_idf")
  }

  private def processNgram(df: DataFrame): DataFrame = {
    import df.sparkSession.implicits._
    val tokensWithTf = df
      .groupBy('note_id, 'token, 'ngram)
      .agg(count('note_id) as "tf")
    val tfidf = tokensWithTf
      .join(model, Seq("token", "ngram"), "left")
      .withColumn("tf_idf", col("tf") * col("idf"))
    tfidf
  }

  private def processTfIdf(df: DataFrame, docCount: Long): DataFrame = {
    import df.sparkSession.implicits._
    val tokensWithDf =
      df.groupBy('token, 'ngram).agg(countDistinct('note_id) as "tokensWithDf")
    val tokensWithIdf = tokensWithDf.withColumn(
      "idf",
      expr(
        "log( (cast(" + docCount.toString + " as double) + 1) / (cast( tokensWithDf as double) + 1) )"
      )
    )
    tokensWithIdf
  }

  def removeStop(df: DataFrame, stop: DataFrame): DataFrame = {
    val tmp = df
      .withColumn("sentenceidx", monotonically_increasing_id())
      .withColumn("words", explode(col("words")))
      .withColumn("wordsidx", monotonically_increasing_id())
      .withColumn("words", lower(col("words")))
      .filter(col("words") rlike "^[\\p{L}-.]+$")
      .filter(!(col("words") rlike "^\\p{Punct}+$"))
      .filter(!(col("words") rlike "^.$"))
      .join(stop, col("words") === col("mot"), "left_anti")
    tmp
  }

  def toArray(df: DataFrame) = {
    import org.apache.spark.sql.expressions.Window
    val tmp = df
      .withColumn(
        "collected",
        collect_list(col("words")).over(
          Window.partitionBy("sentenceidx").orderBy(col("wordsidx").asc)
        )
      )
      .select(col("note_id"), col("sentenceidx"), col("collected"))
      .groupBy(col("note_id"), col("sentenceidx"))
      .agg(max(col("collected")).as("words"))
    tmp

    // df.join(re, Seq("sentenceidx"))
  }

  def ngram(in: Seq[String], num: Int): Seq[String] = {
    in.iterator.sliding(num).withPartial(false).map(_.mkString("_")).toSeq
  }

  val Ngram = udf(ngram _)

  private def process(df: DataFrame): DataFrame = {
    import df.sparkSession.implicits._

    val stp =
      spark.sparkContext.broadcast(stop.collect().map(_.getString(0)).toSet)

    // TODO: readd this
    val note_output =
      df.select(
        'note_id,
        explode(ssplit(coalesce('note_text, lit("")))).as('sentence)
      ).select(
        'note_id,
        'sentence,
        monotonically_increasing_id() as "sentence_id",
        tokenize('sentence).as('words)
      )

    val tokens = cacheParquet(
      note_output
        .selectExpr("*", "posexplode(words)")
        .select('note_id, lower('col) as "token", 'pos, 'sentence_id)
        .filter('token.rlike("^[\\p{L}-.]+$")) // keep only words
        .filter(!'token.rlike("^\\p{Punct}+$")) // only punct
        .filter(!'token.rlike("^.$")) // one letter length
        .join(stop, 'token === 'mot, "left_anti"),
      "tmpidf"
    )

    val out1 = tokens.select('note_id, 'token, lit(1) as "ngram")

    val getWords = udf { structs: Seq[Row] =>
      structs.map { case Row(_: Int, word: String) => word }
    }
    val smallTokens = cacheParquet(
      tokens
        .groupBy("note_id", "sentence_id")
        .agg(
          getWords(sort_array(collect_list(struct('pos, 'token)))) as "words"
        ),
      "smalltokens"
    )

    val out2 = smallTokens
      .withColumn("token", explode(Ngram(col("words"), lit(2))))
      .select('note_id, 'token, lit(2) as "ngram")

    val out3 = smallTokens
      .withColumn("token", explode(Ngram(col("words"), lit(3))))
      .select('note_id, 'token, lit(3) as "ngram")

    out1.union(out2.union(out3))
  }

}

trait Dfcache {

  def cacheParquet(df: DataFrame, file: String): DataFrame = {
    writeParquet(df, file)
    readParquet(df.sparkSession, file)
  }

  def writeParquet(df: DataFrame, file: String): Unit = {
    df.write.format("parquet").mode(SaveMode.Overwrite).save(file)
  }

  def readParquet(spark: SparkSession, file: String): DataFrame = {
    spark.read.format("parquet").load(file).repartition(5000)
  }
}
