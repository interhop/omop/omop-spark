/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.spark.omop.idf

import com.databricks.spark.corenlp.CoreNlpFunctions.{ssplit, tokenize}
import io.frama.parisni.spark.omop.graph.SparkSessionTestWrapper
import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.functions._

class IdfTest extends QueryTest with SparkSessionTestWrapper {

  test("test bigdata zone") {
    import spark.implicits._
    val idfText = List(
      (1, "one flesh one bone one true religion"),
      (2, "all flesh is grass"),
      (3, "one is all all is one")
    )
      .toDF("note_id", "note_text")

    new TfIDF(spark)
      .train(idfText)
      .save("model")
      .fit(idfText, .6, 1.4, 1.5)
      .show(false)

  }

  test("test idf class") {
    import spark.implicits._
    val idfText = ((1, "Je m'appelle bob l'éponge. J'ai le dos qui gratte !")
      :: (2, "Je suis E.T.. Je téléphone à ma [NOM] maison")
      :: (3, "J'ai un 3 ième oeil: c'est personne-achille qui me l'a ouvert.")
      :: (4, "3")
      :: Nil)
      .toDF("note_id", "note_text")

    val test = ((1, "je suis bob l'éponge. Je téléphone à ma maison ?")
      :: Nil)
      .toDF("note_id", "note_text")

    new TfIDF(spark)
      .train(idfText)
      .save("model")
      .fit(idfText, .5, .6, .6)
      .show(false)

    new TfIDF(spark)
      .load("/tmp/model")
      .fit(test, .5, .6, .7)
      .show(false)

  }

  test("test fable") {
    import spark.implicits._
    val idfText =
      spark.sparkContext
        .wholeTextFiles("/tmp/test")
        .toDF
        .toDF("path", "note_text")
        .withColumn("note_text", regexp_replace('note_text, "-", " "))
        .withColumn("note_id", monotonically_increasing_id())

    new TfIDF(spark)
      .train(idfText)
      .fit(idfText, 2, 2, 2)
      .show(false)

  }

  test("test fable trigram") {
    import spark.implicits._
    val df =
      spark.sparkContext
        .wholeTextFiles("/tmp/test")
        .toDF
        .toDF("path", "note_text")
        .withColumn("note_text", regexp_replace('note_text, "-", " "))
        .withColumn("note_id", monotonically_increasing_id())
    val ngram = new BiGramUdf().call _
    spark.udf.register("ngram", ngram)

    val note_output = df
      .select(
        'note_id,
        explode(ssplit(coalesce('note_text, lit("")))) as 'sentence
      )
      .select('note_id, 'sentence, tokenize('sentence) as 'words)

    note_output
      .select(expr("ngram(words, 3)") as "bigram")
      .show(false)

  }
}
