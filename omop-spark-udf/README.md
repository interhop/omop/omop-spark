# OMOP Spark UDF

# NumCover

For each `person`, calculate the number of `visit_occurrence` witch covers temporally.
The function takes the list of start/end pairs of visit plus the number of
hours to tolerate the coverage.


# INSTALL

``` bash
mvn install

# For both scala and java, specify the shaded jar to the spark command line:

# scala
spark-shell --jars omop-spark-udf-0.0.1-SNAPSHOT-shaded.jar

# Python
pySpark --jars omop-spark-udf-0.0.1-SNAPSHOT-shaded.jar

```

# SCALA

``` scala

import org.apache.spark.sql.types._
import io.frama.parisni.omop.udf.NumCover

val numCover = new NumCover().call _
spark.udf.register("numCover", numCover)

val data = spark.read.table("visit_occurrence")
.groupBy("person_id")
.agg(collect_list(array(coalesce(col("visit_start_datetime").cast(LongType),col("visit_end_datetime").cast(LongType),lit(0L)), coalesce(col("visit_end_datetime").cast(LongType),col("visit_start_datetime").cast(LongType),lit(0L))))
.alias("lst_visit"))

data.withColumn("num_cover", expr("numCover(lst_visit, 1)")).show(2, False)
+---------+------------------------------------------------------+---------+
|person_id|lst_visit                                             |num_cover|
+---------+------------------------------------------------------+---------+
|186135932|[[1384176360, 1384262760]]                            |0        |
|186136945|[[1273167360, 1273487400], [1273239360, 1273239420]]  |1        |
+---------+------------------------------------------------------+---------+

```


# PYTHON

``` scala

from pyspark.sql.types import *
from pyspark.sql.functions import *
spark.udf.registerJavaFunction("numCover","io.frama.parisni.omop.udf.NumCover", IntegerType())

data = spark.read.table("visit_occurrence") \
.groupBy("person_id") \
.agg(collect_list(array(coalesce(col("visit_start_datetime").cast(LongType()),col("visit_end_datetime").cast(LongType()),lit(0L)), coalesce(col("visit_end_datetime").cast(LongType()),col("visit_start_datetime").cast(LongType()),lit(0L)))) \
.alias("lst_visit"))

data.withColumn("num_cover", expr("numCover(lst_visit, 1)")).show(2, False)
+---------+------------------------------------------------------+---------+
|person_id|lst_visit                                             |num_cover|
+---------+------------------------------------------------------+---------+
|186135932|[[1384176360, 1384262760]]                            |0        |
|186136945|[[1273167360, 1273487400], [1273239360, 1273239420]]  |1        |
+---------+------------------------------------------------------+---------+

```
