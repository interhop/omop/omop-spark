# Principle

This pipe is **optional**

This `pipe` lookups for a `*_source_concept_id` by joining on a `*_source_value`
and a given `vocabulary_id`.

The lookup is triggered for each `*_source_value` column having a
`constraints.localVocabularyId` associated. Otherwize, the original value is
kept.

