The quality output pipeline is based on the [deequ](https://github.com/awslabs/deequ) library described [in this paper](http://www.vldb.org/pvldb/vol11/p1781-schelter.pdf). 
It produces a quality summary as logs. Right now, the [table schema constraints](https://frictionlessdata.io/specs/table-schema/#field-descriptors) have been implemented:

- required
- unique
- minimum
- maximum
- pattern
- enum

In the future, other basic (integrity constraints, foreign keys...) and complex
(functionnal rule syntax) one.
