
Lookup for standard concept_id
--------------------------------
The lookup is made throught a local_concept_id to the CONCEPT table and the
CONCEPT_RELATIONSHIP table. There is three cases:

#. no lookup: the concept_id get a 0
#. one lookup: the concept_id get its standard id and also a domain
#. multiple lookups: it gets one standard id and a domain per lookup

In case the standard domain is equal to the local domain, the standard
informations will simply be added to the row.
In case the standard domain is different, a new row will be produced
accordingly to the new domain table.

Way 1 **left join** one left join to the concept table produces a table C from
a table B.  => it is important to be able to state witch rows have been
virtualy produced and those who are real information.


Dispatching to other domain
---------------------------
There is multiple dispatching directions. All those table are able to be
dispatched each other.
For each table, only one concept is responsible for the domain. 

- condition_occurence (condition_concept_id)
- procedure_occurence (procedure_concept_id)
- measurement (measurement_concept_id)
- observation (observation_concept_id)
- drug_exposure (drug_concept_id)
- device_exposure (device_concept_id)

Depending on the source and target table, some informations will be lost, and
some will need extenal knowledge to be added.
The OBSERVATION table appears to be the default place to store medical
information since it can ingest a very broad spectrum of values.

Each of the above tables have a **type**. In case the rows have been
dispatched, it will get a special **type_concept_id**. 

- condition -> procedure : condition derived from procedure
- observation -> drug : drug derived from procedure
- X -> Y : Y derived from X

Since new rows are derived from the local rows, there is multiple
implementation solutions.

Dispatching Implementation 1
+++++++++++++++++++++++++++++
The **clocal informations are mooved**.
The limitation of this implementation is the standardized result cannot be used
for local analysis since the local information get modified.

Dispatching Implementation 2
+++++++++++++++++++++++++++++
The **local informations are kept**. When the local concept has no standard
equivalent the standard_concept_id is turned to 0.
When the local concept is moved to an other domain it also get a 0 value in the
local table while the new standard row goes into the standard domain. There is
a need to track the links between the new standard rows and the parent local
rows.  In the best case there is a 1-1 mapping within the same domain, and the
link is built-in within the same row. In case of 1-n mapping within the same
domain, multiple rows are linked to the original local row. In case of 1-n
mapping accross multiple domains, all the standardized rows should be linked to
the original local row.
The FACT_RELATIONSHIP table is the perfect candidate to track those links:
every generated rows as a fact_id_1 and its original as fact_id_2.
This method allows both local and standardized analysis. The standardized
research can be done on the standard concepts which are not equal to 0, while
the local analysis can be done where the local concepts are not equal to 0.


Way 1 **table translation** one translation f is written to produce new rows
from Domain A to Domain B. It may take a dedicated lookup table to add extenal
knowledge.  => it is important to specify which rows are fully tranlated and
those that need extenal knowledge

Generation of ids
++++++++++++++++++

When new rows are produced the new fact_id is unknown. There is two cases:

#. All the new fact_id are generated by the process. This is about every new
row dispatched or every rows duplicated. 
#. All the new fact_id are generated by an external process.




Unit Translation
----------------
Depending on the unit associated to a local concept and its standard concept
associated, the translation function will be different.

- gram
- liter
- meters
- ...

There is a need to produce the translation. UCUM already propose a formalism to
describe this process from a source to a target unit.


De novo tables
--------------
- condition_era
- dose_era
- observation_period
- materialized views


General Design
==============

Structure Files
--------------

Content
+++++++

- tables
  - columns
    - names
    - type
    - nullable
    - comment
    - default value
  - primary keys
  - concept keys
  - foreign
    - table
    - keys
  - partition

Format
++++++

They can be stored as yaml for readability


Usage
+++++

The structure files are used as schema for the source files. The source files
are named accordingly to the tables.
By convention, all the **..._source_concept_id** columns will be look-up to the
**concept** table and the **concept_relationship** table to get the
**..._concept_id** and the **target_domain**.



Configuration File
------------------

- sourceFileFormat: csv|orc|parquet
- targetFileFormat: csv|orc|parquet
- sourceFolder: path
- targetFolder: path
- partitionNumber: 200
