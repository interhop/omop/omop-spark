import yaml
import os
from copy import deepcopy
from typing import List
from typing import Dict


def build_omop_standardize_config(
    path2inputFolder: str,
    path2outputFolder: str,
    path2tmpFolder: str,
    path2yaml_config: str,
    csv_tables: List,
    source_columns: Dict,
    sparkContext) -> None:
    """
    Description: Help build a config file for omop-spark-standardize
    :param path2inputFolder: path to the existing local omop DB
    :param path2outputFolder: path to the final omop DB  to be produced
    :param path2tmpFolder: path to the tmp folder for the intermediate tables
    :param path2yaml: path to the yaml config to be produced
    :param csv_tables: name of the csv tables (read as csv table and not as parquet)
    :param source_columns: dictionnary giving the names of the source columns to be populated. 
        the format is {'omop_table_name': {'source_column_name': local_vocabulary_id}}. 
        e.g.: source_columns = {
                'VISIT_OCCURRENCE': {
                    'visit_source_value': 'SNDS - nature_prestation',
                    'discharge_to_source_value': 'SNDS - pmsi_mouvement_code',
                    'admitted_from_source_value': 'SNDS - pmsi_mouvement_code'
                },
                'CONDITION_OCCURRENCE': {
                    'condition_source_value': 'SNDS - cim10'
                }
            }
    :param sparkContext: sparkContext (created with pyspark.)
    """
    # config header
    config = {
        'appName': 'OMOP standardization pipeline',
        'inputFolder': path2inputFolder,
        'outputFolder': path2outputFolder,
        'tmpFolder': path2tmpFolder,
        'optionalSteps': {
            'localConcept': True,
            'qualityInput': False,
            'qualityOutput': False,
            'standardConcept': True,
            'standardDate': False,
            'standardDispatch': False,
            'standardUnit': False  
        },
        'resources': []
    }
    # default templates for each table
    table_header_template = {
        'inputFormat': 'parquet',
        'outputFormat': 'parquet',
        'active': True
    }
    csv_config_template = {
        'delimiter': ',',
        'quote': '"',
        'escapeQuote': '"',
        'multiline': False
    }
    # iterate over available tables to get schemas
    schemas = {}
    for table_name in os.listdir(path2inputFolder):
        #print(table_name)
        if table_name not in csv_tables:
            schemas[table_name] = sparkContext.read.parquet(os.path.join(path2inputFolder, table_name)).schema
        else:
            schemas[table_name] = sparkContext.read.csv(os.path.join(path2inputFolder, table_name), header=True, inferSchema=True).schema
    
    for table_name, schema in schemas.items():
        table_header = deepcopy(table_header_template)
        table_header['name'] = table_name
        schema_dic = {'fields': []}
        if table_name in csv_tables:
            table_header['csvConfig'] = csv_config_template
            table_header['inputFormat'] = 'csv'
        for field in schema.fields:
            variable_infos = {'type': field.dataType.simpleString(), 'name': field.name}
            # populate constraint for source columns
            if table_name in source_columns.keys():
                if field.name in source_columns[table_name].keys():
                    variable_infos['constraints'] = {
                        'localVocabularyId': source_columns[table_name][field.name]
                    }
            schema_dic['fields'].append(variable_infos)
        table_header['schema'] = schema_dic
        config['resources'].append(table_header)
        
    # write yaml
    with open(path2yaml_config, 'w') as f:
        yaml.dump(config, f, default_flow_style=False)
    return None