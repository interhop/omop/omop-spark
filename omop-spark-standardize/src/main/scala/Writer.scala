/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  *     This file is part of OMOP-SPARK.
  *
  *     OMOP-SPARK is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     OMOP-SPARK is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.interchu.omop
import io.frama.interchu.omop.ConfigYaml._

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.SaveMode._
import org.apache.spark.sql.SparkSession
import java.nio.file.Paths
import io.frama.parisni.spark.csv.CSVTool
import io.frama.parisni.spark.dataframe.DFTool

object Writer {

  /**
    *  Write all the the data
    *  Also:
    */

  def writeAll(
      dfMap: Map[String, DataFrame],
      conf: Config,
      structs: Map[String, StructType],
      key: String
  ): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()

    dfMap
      .filter(f => f._1 != "concept_pivot")
      .foreach(f => {
        if (conf.resources.filter(r => r.name == f._1)(0).active) { // is the re
          result(f._1) = write(
            f._2,
            conf,
            conf.resources.filter(p => p.name == f._1)(0),
            structs.get(f._1).get,
            key
          )
        }
      })

    dfMap ++ result.toMap
  }

  def write(
      df: DataFrame,
      conf: Config,
      resources: Resources,
      schema: StructType,
      key: String
  ): DataFrame = {
    val file = Paths.get(conf.outputFolder, resources.name).toString

    resources.outputFormat match {
      case "csv" =>
        CSVTool.write(DFTool.reorderColumns(df, schema), file, Overwrite)
      case "orc" =>
        DFTool
          .reorderColumns(df, schema)
          .write
          .format("orc")
          .mode(Overwrite)
          .save(file)
      case "parquet" =>
        DFTool
          .reorderColumns(df, schema)
          .write
          .format("parquet")
          .mode(Overwrite)
          .save(file)
      case "hive" =>
        DFTool
          .reorderColumns(df, schema)
          .write
          .format("parquet")
          .mode(Overwrite)
          .saveAsTable(conf.outputFolder + "." + resources.name + "_standard")
    }

    resources.outputFormat match {
      case "csv" =>
        df.sparkSession.read
          .format("csv")
          .schema(df.schema)
          .option("multiline", true)
          .option("delimiter", ",")
          .option("header", false)
          .option("quote", "\"")
          .option("escape", "\"")
          .option("timestampFormat", "yyyy-MM-dd HH:mm:ss")
          .option("dateFormat", "yyyy-MM-dd")
          .option("mode", "FAILFAST")
          .load(file)
      case "orc"     => df.sparkSession.read.orc(file)
      case "parquet" => df.sparkSession.read.parquet(file)
      case "hive" =>
        df.sparkSession.table(
          conf.outputFolder + "." + resources.name + "_standard"
        )
    }
  }
}
