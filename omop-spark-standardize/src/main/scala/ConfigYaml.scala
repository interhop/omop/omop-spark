/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  *     This file is part of OMOP-SPARK.
  *
  *     OMOP-SPARK is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     OMOP-SPARK is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.interchu.omop

import net.jcazevedo.moultingyaml._
import java.io.{File, FileInputStream}
import scala.io.Source

object ConfigYaml extends DefaultYamlProtocol {

  case class Constraints(
      required: Option[Boolean],
      unique: Option[Boolean],
      pattern: Option[String],
      minimum: Option[Long],
      maximum: Option[Long],
      enum: Option[Array[String]],
      localVocabularyId: Option[String]
  )

  case class Fields(
      name: String,
      `type`: String,
      constraints: Option[Constraints] = None
  )

  case class Steps(
      qualityInput: Boolean,
      qualityOutput: Boolean,
      standardConcept: Boolean,
      localConcept: Boolean,
      standardDispatch: Boolean,
      standardDate: Boolean,
      standardUnit: Boolean
  )

  case class Schema(fields: List[Fields])

  case class ForeignKeysReference(fields: Array[String], resource: String)

  case class ForeignKeys(fields: Array[String], reference: ForeignKeysReference)

  case class Resources(
      name: String,
      partition: Option[Int],
      inputFormat: String,
      outputFormat: String,
      active: Boolean,
      csvConfig: Option[CsvConfig],
      schema: Schema,
      foreignKeys: Option[List[ForeignKeys]]
  ) {
    require(
      csvConfig.isDefined && inputFormat == "csv" || inputFormat != "csv",
      "csv configuration applies only for csv type"
    )
    require(
      List("csv", "orc", "parquet", "hive").contains(inputFormat),
      "input format can only be csv, orc or parquet"
    )
    require(
      List("csv", "orc", "parquet", "hive").contains(outputFormat),
      "output format can only be csv, orc or parquet"
    )
  }

  case class CsvConfig(
      delimiter: String,
      quote: String,
      escapeQuote: String,
      multiline: Boolean
  )

  case class DispatchFields(
      name: String,
      from: Option[String],
      `type`: Option[String]
  )

  case class DispatchTo(
      name: String,
      standardDomain: String,
      dispatchFields: List[DispatchFields]
  ) {}
  case class DispatchFrom(
      from: String,
      localDomain: String,
      columnDispatch: String,
      to: List[DispatchTo]
  ) {}
  case class Config(
      appName: String,
      inputFolder: String,
      outputFolder: String,
      tmpFolder: String,
      optionalSteps: Steps,
      dispatch: Option[List[DispatchFrom]],
      resources: List[Resources]
  ) {
    require(
      resources.map(_.name).contains("concept_pivot"),
      "resources should contain one table named 'concept_pivot'"
    )

  }

  implicit val constraintFormat = yamlFormat7(Constraints)
  implicit val fieldsFormat = yamlFormat3(Fields)
  implicit val schemaFormat = yamlFormat1(Schema)
  implicit val csvConfigFormat = yamlFormat4(CsvConfig)
  implicit val foreignKeyReferenceFormat = yamlFormat2(ForeignKeysReference)
  implicit val foreignKeyFormat = yamlFormat2(ForeignKeys)
  implicit val resourcesFormat = yamlFormat8(Resources)
  implicit val stepsFormat = yamlFormat7(Steps)
  implicit val dispatchFieldsFormat = yamlFormat3(DispatchFields)
  implicit val dispatchToFormat = yamlFormat3(DispatchTo)
  implicit val dispatchFromFormat = yamlFormat4(DispatchFrom)
  implicit val configFormat = yamlFormat7(Config)

  def getFromString(ymlTxt: String): Config = {
    val yaml = ymlTxt.stripMargin.parseYaml
    val conf = yaml.convertTo[Config]
    conf
  }

  def getFromPath(filename: String): Config = {

    val ymlTxt = Source.fromFile(filename).mkString
    getFromString(ymlTxt)

  }
}
