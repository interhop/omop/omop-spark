/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  * This file is part of OMOP-SPARK.
  *
  * OMOP-SPARK is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * OMOP-SPARK is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.interchu.omop

import io.frama.interchu.omop.ConfigYaml._

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import java.nio.file.Paths
import io.frama.parisni.spark.csv.CSVTool
import io.frama.parisni.spark.dataframe.DFTool
import com.typesafe.scalalogging.LazyLogging

object Reader extends LazyLogging {

  /**
    * Read all the **active** dataframe
    * and produce a map of dataframes
    * Also:
    *  - remove additional columns
    *  - add missing columns
    *  - add default values
    *  - verify required columns
    */

  def readAll(
      spark: SparkSession,
      conf: Config,
      dfMap: Map[String, DataFrame]
  ): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()
    dfMap
      .foreach(f => {
        if (
          conf.resources
            .filter(p => {
              p.name == f._1
            })(0)
            .active
        )
          result(f._1) = read(
            spark,
            conf,
            conf.resources.filter(p => {
              p.name == f._1
            })(0),
            f._2.schema
          )
      })

    dfMap ++ result.toMap
  }

  def read(
      spark: SparkSession,
      conf: Config,
      resources: Resources,
      schema: StructType
  ): DataFrame = {
    val file = Paths.get(conf.inputFolder, resources.name)
    logger.warn(file.toString())
    var result = resources.inputFormat match {
      case "csv" =>
        CSVTool(
          spark,
          path = file.toString(),
          schema = schema,
          delimiter = Some(resources.csvConfig.get.delimiter),
          escape = Some(resources.csvConfig.get.escapeQuote),
          multiline = Some(resources.csvConfig.get.multiline),
          isCast = true
        )
      case "orc" =>
        DFTool.applySchemaSoft(
          spark.read.format("orc").load(file.toString()),
          schema
        )
      case "parquet" =>
        DFTool.applySchemaSoft(
          spark.read.format("parquet").load(file.toString()),
          schema
        )
      case "hive" =>
        DFTool.applySchemaSoft(
          spark.read.table(conf.inputFolder + "." + resources.name),
          schema
        )
    }
    if (resources.name == "concept_pivot")
      result.persist()
    if (resources.partition.isDefined)
      result.repartition(resources.partition.get)
    else
      result
  }

}
