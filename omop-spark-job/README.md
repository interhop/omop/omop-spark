# OMOP-SPARK-JOB

spark-jobserver needs a bintray maven repository to be setup in the
.m2/setting.xml, add this :

```xml
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray</id>
                    <name>bintray</name>
                    <url>https://dl.bintray.com/spark-jobserver/maven</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray</id>
                    <name>bintray</name>
                    <url>https://dl.bintray.com/spark-jobserver/maven</url>
                </pluginRepository>
            </pluginRepositories>
            <id>bintray</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>bintray</activeProfile>
    </activeProfiles>
```


# QUERY JOB SERVER

## Principle

An dedicated spark-context wait for REST queries from the FHIR API. Because it is configured with a *FAIR* scheduler, it can handle multiple query in parallel. 

## Caching

There is two level of caching.

### Query Caching Level

If a query has allready been run recently (say within the 24h) then it is not computed again. The query engine will return back the previous result.
It the delay is exceeded, then a new processing will occur.

### Leaf Caching Level

If a Leaf (a resource Element) has allready been precessed recently (say 1h), then it won´t be fetched again from SOLR, but cached within the spark-context. This adds more memory footprint but speeds-up a try/error succesion of querieswith overall the same leaves.


## Syntax Elements

Every objects are json with à `_type` element, which indicate its type. The implemented types are :
- resource: A fhir resource with its own `filterSolr` element. They are the `Leaves` of the tree.
- InnerJoin: Equivalent to a AND
- Union: Equivalent to a OR
- AntiJoin: Equivalent to a EXEPT
- NamongM: N among M, is equivalent to a OR, but you can specify how much of every part you want from the M elements.
- TemporalJoin: TODO



### Fhir Resources

The `filterFhir` syntax is passed to the FHIR API query endpoint to be transformed into a `filterSolr` syntax.

There is also the optional `numOccurrences` field which defines how many minimum occurrences one need for this resource.


### Operators

For the `InnerJoin`, `Union`, `AntiJoin` operators, the number of childs is >= 1. A child is a list of any `Element` including resource and operators themselfs.

The `NamongM` operator must have >= 3 elements and also `n` must be < to `m`. Otherwize one can use `Union` or `InnerJoin` operator respectively.


## Example

### Simple example
The simplest example is a single resource.

This is the front-end client request:
```json
{
  "_type": "resource",
  "resourceType": "Patient",
  "filterFhir": "name=bob"
}
```

this is translated by the FHIR API query endpoint into:
```json
{
  "_type": "resource",
  "resourceType": "Patient",
  "collectionSolr": "patientAphp",
  "filterFhir": "name=bob",
  "filterSolr": "name:bob"
}
```

### Nested example
You can get any level of nesting. By the way, you can embed a previous query within a new query and so on.

```json
{
  "_type": "NamongM",
  "count": 2,
  "child": [
    {
      "_type": "InnerJoin",
      "child": [
        {
          "_type": "resource",
          "resourceType": "Patient",
          "filterSolr": "df1",
          "numOccurrences": 3,
        },
        {
          "_type": "resource",
          "resourceType": "Condition",
          "filterSolr": "df2"
        }
      ]
    },
    {
      "_type": "Union",
      "child": [
        {
          "_type": "resource",
          "resourceType": "Observation",
          "filterSolr": "df1"
        },
        {
          "_type": "resource",
          "resourceType": "Encounter",
          "filterSolr": "df2"
        }
      ]
    },
    {
      "_type": "AntiJoin",
      "child": [
        {
          "_type": "resource",
          "resourceType": "Patient",
          "filterSolr": "df3"
        },
        {
          "_type": "resource",
          "resourceType": "Patient",
          "filterSolr": "df4"
        }
      ]
    }
  ]
}
```
