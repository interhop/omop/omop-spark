/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job

import com.opentable.db.postgres.junit.{
  EmbeddedPostgresRules,
  SingleInstancePostgresRule
}
import io.frama.parisni.spark.postgres.PGTool
import org.apache.spark.sql.SparkSession
import org.junit.Rule

import scala.annotation.meta.getter

trait SparkSessionTestWrapper {

  // looks like crazy but compatibility issue with junit rule (public)
  @(Rule @getter)
  var pg: SingleInstancePostgresRule = EmbeddedPostgresRules.singleInstance()

  def getPgUrl =
    pg.getEmbeddedPostgres.getJdbcUrl(
      "postgres",
      "postgres"
    ) + "&currentSchema=public"

  def getPgTool() = PGTool(spark, getPgUrl, "/tmp")

  def getConn() = getPgTool.getConnection()

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
