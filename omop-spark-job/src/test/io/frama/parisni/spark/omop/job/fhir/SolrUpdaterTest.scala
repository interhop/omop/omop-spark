/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job.fhir

import java.sql.{Timestamp}

import com.lucidworks.spark.util.SolrSupport
import io.frama.parisni.spark.omop.job.{
  PostgresUtils,
  SolrConfTest,
  SolrUtils,
  SparkSessionTestWrapper
}
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.junit.Test

class SolrUpdaterTest
    extends SolrConfTest
    with SparkSessionTestWrapper
    with PostgresUtils
    with SolrUtils {

  @Test
  def testPgToSolr(): Unit = {

    // SOLR
    startSolrCloudCluster
    createSolrTables()

    val options = Map(
      "zkhost" -> zkHost,
      "commit_within" -> "10000",
      "batch_size" -> "5000",
      "timezone_id" -> "Europe/Paris",
      "collection" -> "source",
      "request_handler" -> "/export"
    )
    val cloudSolrClient = SolrSupport.getCachedCloudClient(zkHost)
    cloudSolrClient.setDefaultCollection("source")

    val ids = List("1", "2", "3")
    val field = "list"
    val value = List(1L, 2L, 3L)

    // POSTGRES

    import spark.implicits._

    // Create table "source"
    val s_inputDF: DataFrame = ((
      1L,
      "id1s",
      "PG details of 1st row source",
      Timestamp.valueOf("2016-02-01 23:00:01"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
        2L,
        "id2s",
        "PG details of 2nd row source",
        Timestamp.valueOf("2017-06-05 23:00:01"),
        Timestamp.valueOf("2016-06-16 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00")
      ) ::
      (
        3L,
        "id3s",
        "PG details of 3rd row source",
        Timestamp.valueOf("2017-08-07 23:00:01"),
        Timestamp.valueOf("2016-06-16 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00")
      ) ::
      (
        4L,
        "id4s",
        "PG details of 4th row source",
        Timestamp.valueOf("2018-10-16 23:00:01"),
        Timestamp.valueOf("2016-06-16 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00")
      ) ::
      (
        5L,
        "id5",
        "PG details of 5th row source",
        Timestamp.valueOf("2019-12-27 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00")
      ) ::
      (
        6L,
        "id6",
        "PG details of 6th row source",
        Timestamp.valueOf("2020-01-14 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00"),
        Timestamp.valueOf("2016-06-16 00:00:00")
      ) ::
      Nil).toDF(
      "id",
      "pk2",
      "details",
      "date_update",
      "date_update2",
      "date_update3"
    )

    s_inputDF.write
      .format("postgres")
      .mode(SaveMode.Overwrite)
      .option("url", getPgUrl)
      .option("type", "full")
      .option("table", "mytable")
      .option("partitions", 4)
      .option("kill-locks", true)
      .save

    val conn = getConn()

    def updateSolr(ids: List[Long]) =
      updateAtomicBatch(cloudSolrClient, ids.map(_.toString), field, value)

    val query = "SELECT * FROM mytable"
    process(conn, query, fetchSize = 3, updateSolr)

    val result = spark.read.format("solr").options(options).load("source")
    val assertion = result.select($"$field" like "1, " + value.mkString(", "))
    assert(assertion.collect().map(_.getBoolean(0)).forall(p => p))
  }
}
