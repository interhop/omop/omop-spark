/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job.solr.update

import com.lucidworks.spark.util.SolrSupport
import io.frama.parisni.spark.omop.job._
import org.apache.spark.sql.functions._

class SolrAtomicUpdateTest extends SolrConfTest with SolrUtils {

  test("test solr update atomic batch") {

    startSolrCloudCluster
    createSolrTables()

    val options = Map(
      "zkhost" -> zkHost,
      "commit_within" -> "10000",
      "batch_size" -> "5000",
      "timezone_id" -> "Europe/Paris",
      "collection" -> "source",
      "request_handler" -> "/export"
    )
    val cloudSolrClient = SolrSupport.getCachedCloudClient(zkHost)
    cloudSolrClient.setDefaultCollection("source")

    val ids = List("1", "2", "3")
    val field = "list"
    val value = List(1L, 2L, 3L)

    updateAtomicBatch(cloudSolrClient, ids, field, value)

    val result = spark.read.format("solr").options(options).load("source")
    import spark.implicits._
    assert(
      result
        .filter("id = 1")
        .select(size(split($"list", ",")))
        .collect
        .map(_.getInt(0))
        .head === 4
    )
  }

}
