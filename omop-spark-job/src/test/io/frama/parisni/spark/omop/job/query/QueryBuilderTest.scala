package io.frama.parisni.spark.omop.job.query

import org.apache.spark.sql.QueryTest

case class Patient(patient: Int)
class QueryBuilderTest extends QueryTest with QueryDataset {

  test("basic") {

    val pat1 = BasicResource("Patient", "df1")
    val pat2 = BasicResource("Patient", "df2")
    val pat3 = BasicResource("Patient", "df3")
    val pat4 = BasicResource("Patient", "df4")
    val lefJoin1: BaseObject = InnerJoin(List(pat1, pat3))
    val lefJoin2: BaseObject = InnerJoin(List(pat2, pat3))
    val lefJoin3: BaseObject = InnerJoin(List(pat1, pat4, lefJoin1, lefJoin2))
    val union1: BaseObject =
      AntiJoin(List(lefJoin3, Union(List(pat1, pat2, pat3))))
    val nAmongM = NamongM(2, List(pat3, pat2, pat1))
    val nAmongM2 = NamongM(2, List(lefJoin1, lefJoin2, pat3))
    val anti1 = AntiJoin(List(pat3, pat2, pat1))
    val union2 = Union(List(union1, pat3))

    assert(QueryBuilder.getQuery(lefJoin1).df.count === 1)
    assert(QueryBuilder.getQuery(lefJoin2).df.count === 2)
    assert(QueryBuilder.getQuery(lefJoin3).df.count === 1)

    assert(QueryBuilder.getQuery(nAmongM).df.count === 2)
    assert(QueryBuilder.getQuery(nAmongM2).df.count === 2)

    assert(QueryBuilder.getQuery(anti1).df.count === 2)
    assert(QueryBuilder.getQuery(union1).df.count === 0)
    assert(QueryBuilder.getQuery(union2).df.count === 3)
  }

  test("basic parser") {
    val goal =
      "NamongM(2,List(InnerJoin(List([Patient, df1], [Condition, df2])), Union(List([Observation, df1], [Encounter, df2])), AntiJoin(List([Patient, df3], [Patient, df4]))))"
    assert(QueryParser.parse(exampleJson).toString === goal)
  }

  test("parser + query builder") {
    val parsed = QueryParser.parse(exampleJson)
    val goal = spark.createDataFrame(Patient(1) :: Patient(1) :: Nil)
    checkAnswer(goal, QueryBuilder.getQuery(parsed).df)
  }

}
