package scala.io.frama.parisni.spark.omop.job.play

import play.api.libs.json.Json
import org.junit.Test

/**
  * see https://github.com/playframework/play-json/blob/e5f0a8ad663747b2e12d89c605362cc4779e5d34/docs/manual/working/scalaGuide/main/json/code/ScalaJsonAutomatedSpec.scala
  */
class ScalaJsonAutomatedSpec {

  val test =
    """{
  "type": "union",
  "childs": [
    {
      "type": "resource",
      "resourceType": "Patient",
      "filter": "the filter"
    },
    {
      "type": "antiJoin",
      "childs": [
        {
          "type": "resource",
          "resourceType": "Encounter",
          "filter": "the filter"
        },
        {
          "type": "innerJoin",
          "childs": [
            {
              "type": "resource",
              "resourceType": "Medication",
              "filter": "the filter"
            },
            {
              "type": "amongJoin",
              "count": 2,
              "childs": [
                {
                  "type": "resource",
                  "resourceType": "Patient",
                  "filter": "the filter"
                },
                {
                  "type": "resource",
                  "resourceType": "Enconter",
                  "filter": "the filter"
                },
                {
                  "type": "resource",
                  "resourceType": "Observation",
                  "filter": "the filter"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}"""

  @Test
  def bobTest {

    val contributorJson = Json.parse("""
        {
          "admTpe":"contributor",
          "organization":"Foo"
        }
      """)
    val clientJson =
      Json.parse("""
        {
          "admTpe":"client",
          "organization":"Bar",
          "child": [
             {"admTpe":"client","organization":"bar"},
             {"admTpe":"contributor","organization":"bar"}
          ]
        }
      """)
    //#trait-custom-representation

    //#auto-JSON-custom-trait
    import play.api.libs.json._

    implicit val cfg = JsonConfiguration(
      // Each JSON objects is marked with the admTpe, ...
      discriminator = "admTpe",
      // ... indicating the lower-cased name of sub-type
      typeNaming = JsonNaming { fullName =>
        fullName.drop(66 /* remove pkg */ ).toLowerCase
      }
    )

    // Finally able to generate format for the sealed family 'Role'
    // First provide instance for each sub-types 'client' and 'Contributor':
    implicit val contributorRead = Json.reads[Contributor]
    implicit lazy val clientRead = Json.reads[Client]
    implicit val roleRead = Json.reads[Role]

    //#auto-JSON-custom-trait

    def readAnyRole(input: JsValue): JsResult[Role] = input.validate[Role]
    def getRole(result: Role) = result match {
      case e: Contributor => println("contributor")
      case e: Client      => println("client"); println(e.toString)
      case _              => println("jim")
    }

    getRole(readAnyRole(contributorJson).get)
    getRole(readAnyRole(clientJson).get)

  }

  sealed trait Role

  case class Contributor(organization: String) extends Role

  case class Client(organization: String, child: Option[Seq[Client]])
      extends Role

  case class Query(`type`: String,
                   organization: String,
                   child: Option[List[Query]])

}
