/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job

import java.io.File
import java.sql.Timestamp

import com.lucidworks.spark.util.SolrSupport
import org.apache.solr.cloud.MiniSolrCloudCluster
import org.apache.solr.common.cloud.ZkConfigManager
import org.apache.spark.sql.SaveMode.Overwrite
import org.apache.spark.sql.{DataFrame, QueryTest}
import org.eclipse.jetty.servlet.ServletHolder
import org.junit.Assert.assertTrue
import org.restlet.ext.servlet.ServerServlet

class SolrConfTest
    extends QueryTest
    with SparkSessionTestWrapper
    with SolrCloudTestBuilder {

  def createSolrTables(): Unit = { // Uses JUnit-style assertions

    import spark.implicits._

    startSolrCloudCluster()
    val solrCloudClient = SolrSupport.getCachedCloudClient(zkHost)

    // Create table "source"
    val s_inputDF: DataFrame = ((
      "1",
      "id1s",
      "Solr details of 1st row source",
      Timestamp.valueOf("2016-02-01 23:00:01"),
      Array(1L)
    ) ::
      (
        "2",
        "id2s",
        "Solr details of 2nd row source",
        Timestamp.valueOf("2017-06-05 23:00:01"),
        Array(1L)
      ) ::
      (
        "3",
        "id3s",
        "Solr details of 3rd row source",
        Timestamp.valueOf("2017-08-07 23:00:01"),
        Array(1L)
      ) ::
      (
        "4",
        "id4s",
        "Solr details of 4th row source",
        Timestamp.valueOf("2018-10-16 23:00:01"),
        Array(1L)
      ) ::
      (
        "5",
        "id5",
        "Solr details of 5th row source",
        Timestamp.valueOf("2019-12-27 00:00:00"),
        Array(1L)
      ) ::
      (
        "6",
        "id6",
        "Solr details of 6th row source",
        Timestamp.valueOf("2020-01-14 00:00:00"),
        Array(1L)
      ) ::
      Nil).toDF("id", "pk2", "details", "date_update", "list")

    val s_collectionName = "source"
    SolrCloudUtil.buildCollection(
      zkHost,
      s_collectionName,
      null,
      1,
      cloudClient,
      spark.sparkContext
    )
    val s_solrOpts = Map(
      "zkhost" -> zkHost,
      "collection" -> s_collectionName,
      "flatten_multivalued" -> "false" // for correct reading column "date"
    )
    s_inputDF.write.format("solr").options(s_solrOpts).mode(Overwrite).save()

    // Explicit commit to make sure all docs are visible
    solrCloudClient.commit(s_collectionName, true, true)
  }

  def createSolrCohortTables(): Unit = { // Uses JUnit-style assertions

    startSolrCloudCluster()
    val solrCloudClient = SolrSupport.getCachedCloudClient(zkHost)

    val s_collectionName = "groupAphp"
    val s_solrOpts = Map(
      "zkhost" -> zkHost,
      "collection" -> s_collectionName,
      "flatten_multivalued" -> "false" // for correct reading column "date"
    )
    SolrCloudUtil.createCollection(
      "groupAphp",
      2,
      1,
      1,
      "groupAphp",
      new File("src/test/resources/groupAphp/conf/"),
      this.cloudClient
    )

    // Explicit commit to make sure all docs are visible
    solrCloudClient.commit(s_collectionName, true, true)
  }

  def startSolrCloudCluster(): Unit = {

    System.setProperty("jetty.testMode", "true")
    val solrXml = new File("src/test/resources/solr.xml")
    val solrXmlContents: String =
      TestSolrCloudClusterSupport.readSolrXml(solrXml)

    val targetDir = new File("target")
    if (!targetDir.isDirectory)
      fail(
        "Project 'target' directory not found at :" + targetDir.getAbsolutePath
      )

    testWorkingDir =
      new File(targetDir, "scala-solrcloud-" + System.currentTimeMillis)
    if (!testWorkingDir.isDirectory)
      testWorkingDir.mkdirs

    // need the schema stuff
    val extraServlets: java.util.SortedMap[ServletHolder, String] =
      new java.util.TreeMap[ServletHolder, String]()

    val solrSchemaRestApi: ServletHolder =
      new ServletHolder("SolrSchemaRestApi", classOf[ServerServlet])
    solrSchemaRestApi.setInitParameter(
      "org.restlet.application",
      "org.apache.solr.rest.SolrSchemaRestApi"
    )
    extraServlets.put(solrSchemaRestApi, "/schema/*") //delete \ before *

    cluster = new MiniSolrCloudCluster(
      2,
      null /* hostContext */,
      testWorkingDir.toPath(),
      solrXmlContents,
      extraServlets,
      null
    )
    cloudClient = cluster.getSolrClient
    cloudClient.connect()

    assertTrue(
      !cloudClient.getZkStateReader.getClusterState.getLiveNodes.isEmpty
    )
    zkHost = cluster.getZkServer.getZkAddress
    println("zkHost = " + zkHost)

    // skClient config
    val zkClient = cloudClient.getZkStateReader.getZkClient
    val zkConfigManager = new ZkConfigManager(zkClient)
    val confName = "testConfig"
    val confDir = new File("src/test/resources/conf")
    zkConfigManager.uploadConfigDir(confDir.toPath, confName)

  }
}
