package io.frama.parisni.spark.omop.job.query

import io.frama.parisni.spark.query._
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions._

abstract class BaseObject(objectType: String)

abstract class QueryObject(objectType: String, sameEncounter: Boolean = false)
    extends BaseObject(objectType)

abstract class Entity(entityType: String, filter: String)
    extends BaseObject("entity")

case class BasicResource(resourceType: String,
                         filter: String,
                         numOccurrences: Option[Short] = None)
    extends Entity("basic_resource", filter) {
  override def toString: _root_.java.lang.String = {
    s"[$resourceType, $filter]"
  }
}

case class InnerJoin(queryObject: List[BaseObject],
                     sameEncounter: Boolean = false)
    extends QueryObject("inner_join", sameEncounter)

case class NamongM(n: Short,
                   queryObject: List[BaseObject],
                   sameEncounter: Boolean = false)
    extends QueryObject("among_outer_join", sameEncounter)

case class AntiJoin(queryObject: List[BaseObject],
                    sameEncounter: Boolean = false)
    extends QueryObject("anti_join", sameEncounter)

case class Union(queryObject: List[BaseObject], sameEncounter: Boolean = false)
    extends QueryObject("union", sameEncounter)

object QueryBuilder {

  def getQuery(queryObject: BaseObject)(
      implicit dfGetter: BasicResource => Query): Query = {

    queryObject match {

      case patient: BasicResource => dfGetter(patient).alias(getUid)

      case innerJoin: InnerJoin => // l'objet left join contient une liste d'elements
        innerJoin.queryObject match {
          case List(unique) => // si c´est une liste a un seul element
            unique match {
              case ent: BasicResource =>
                dfGetter(ent).alias(getUid)
              case query: QueryObject =>
                getQuery(query).alias(getUid)
            }
          case head :: tail => // si c'est une liste multiple
            head match {
              case ent: BasicResource => // si c'est une entité
                Query((Query(
                        getQuery(InnerJoin(tail)).select(skipDuplicates = true),
                        getUid) + dfGetter(ent)
                        .alias(getUid)
                        .on("patient")).select(skipDuplicates = true),
                      getUid)
                  .alias(getUid) // on left join le df avec ce qui reste
              case query: QueryObject => // si c´est une autre query
                Query(
                  (getQuery(query)
                    .alias(getUid) +
                    Query(
                      getQuery(InnerJoin(tail)).select(skipDuplicates = true),
                      getUid)
                      .alias(getUid)
                      .on("patient")).select(skipDuplicates = true),
                  getUid
                ).alias(getUid)
            }
        }

      case antiJoin: AntiJoin => // l'objet left join contient une liste d'elements
        antiJoin.queryObject match {
          case List(unique) => // si c´est une liste a un seul element
            unique match {
              case ent: BasicResource =>
                dfGetter(ent).alias(getUid)
              case query: QueryObject =>
                getQuery(query).alias(getUid)
            }
          case head :: tail => // si c'est une liste multiple
            head match {
              case ent: BasicResource => // si c'est une entité
                (dfGetter(ent) - getQuery(AntiJoin(tail))
                  .alias(getUid)
                  .on("patient"))
                  .alias(getUid) // on left join le df avec ce qui reste
              case query: QueryObject => // si c´est une autre query
                (getQuery(query)
                  .alias(getUid) -
                  getQuery(AntiJoin(tail))
                    .alias(getUid)
                    .on("patient")).alias(getUid)
            }
        }

      case union: Union => // Union
        union.queryObject match {
          case List(unique) => // si c´est une liste a un seul element
            unique match {
              case ent: BasicResource =>
                dfGetter(ent).alias(getUid)
              case query: QueryObject =>
                getQuery(query).alias(getUid)
            }
          case head :: tail => // si c'est une liste multiple
            head match {
              case ent: BasicResource => // si c'est une entité
                (getQuery(Union(tail)) & dfGetter(ent)
                  .alias(getUid))
                  .alias(getUid) // on left join le df avec ce qui reste
              case query: QueryObject => // si c´est une autre query
                (DataFrameQuery(
                  getQuery(query)
                    .alias(getUid)
                    .select(skipDuplicates = true), // il faut supprimer les doublons pour faire un union
                  getUid) &
                  DataFrameQuery(getQuery(InnerJoin(tail))
                                   .alias(getUid)
                                   .select(skipDuplicates = true),
                                 getUid)).alias(getUid)
            }
        }

      case namongM: NamongM => // N parmis M
        namongM.queryObject match {
          case smallList: List[QueryObject]
              if smallList.size < 3 => // si c´est une liste inferieure à 3
            throw new Exception("shall be at least 3 elements")
          case smallList: List[QueryObject]
              if namongM.n >= smallList.size => // si c´est une liste inferieure à 3
            throw new Exception("n shall be inferior to m")
          case first :: largeList =>
            val res = getQuery(first)
              .alias(getUid)
              .select(skipDuplicates = true)
            var tmp: Query =
              Query(res.withColumn("full_id", res("patient")), getUid)
                .alias(getUid)
            var predicate: Column = when(tmp.df("patient").isNotNull, 1)
            for (element <- largeList) {
              val query = Query(getQuery(element).select(skipDuplicates = true),
                                getUid).alias(getUid)
              tmp = fullJoin(tmp, query).alias(getUid)
              predicate = predicate + when(query.df(s"patient").isNotNull, 1)
                .otherwise(0)
            }
            Query( // only keep the full id
                  (tmp | predicate >= namongM.n).df
                    .select(col("full_id").as("patient")),
                  getUid)
        }
    }
  }

  protected def fullJoin(left: Query, right: Query): Query = {
    Query(
      FullOuterJoin(left, right, left("full_id") === right("patient")).df
        .withColumn("full_id", coalesce(left("full_id"), right("patient"))),
      getUid
    )
  }

  protected def getUid =
    java.util.UUID.randomUUID().toString.replaceAll("-.*", "")

}
