package io.frama.parisni.spark.omop.job.query

case class GenericQuery(_type: String,
                        resourceType: Option[String],
                        filterFhir: Option[String],
                        filterSolr: Option[String],
                        count: Option[Short],
                        numOccurrences: Option[Short],
                        child: Option[List[GenericQuery]])
object QueryParser {

  def parse(json: String): BaseObject = {
    import play.api.libs.json._
    implicit lazy val queryRead = Json.reads[GenericQuery]
    specJson(Json.parse(json).validate[GenericQuery].get)
  }

  protected def specJson(q: GenericQuery): BaseObject = {
    q._type match {
      case "resource" =>
        BasicResource(resourceType = q.resourceType.get,
                      filter = q.filterSolr.get,
                      numOccurrences = q.numOccurrences)
      case "InnerJoin" =>
        InnerJoin(queryObject = q.child.get.map(x => specJson(x)))
      case "AntiJoin" =>
        AntiJoin(queryObject = q.child.get.map(x => specJson(x)))
      case "Union" =>
        Union(queryObject = q.child.get.map(x => specJson(x)))
      case "NamongM" =>
        NamongM(q.count.get, queryObject = q.child.get.map(x => specJson(x)))
    }
  }
}
