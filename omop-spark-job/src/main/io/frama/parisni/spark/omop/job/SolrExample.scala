/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job

import com.typesafe.config.Config
import org.apache.spark.sql.SparkSession
import org.scalactic._
import spark.jobserver.SparkSessionJob
import spark.jobserver.api.{JobEnvironment, SingleProblem, ValidationProblem}
import com.lucidworks.spark.util.SolrDataFrameImplicits._
import io.frama.parisni.spark.omop.job.group.OmopTools
import io.frama.parisni.spark.omop.job.query.{
  BasicResource,
  QueryBuilder,
  QueryParser,
  QuerySolver
}
import io.frama.parisni.spark.postgres.PGTool
import play.api.libs.json.{JsValue, Json}

import scala.util.Try
import play.api.libs.json._
import play.api.libs.functional.syntax._

case class Query(
    cohortDefinitionName: String,
    cohortDefinitionDescription: Option[String],
    cohortDefinitionSyntax: String,
    ownerEntityId: Long,
    delayInHours: Int
)

object SolrExample extends SparkSessionJob {
  type JobData = String
  type JobOutput = Array[Map[String, Any]]

  override def runJob(
      spark: SparkSession,
      runtime: JobEnvironment,
      data: JobData
  ): JobOutput = {

    val query: Query = parse(data).get

    val envir = getEnvir(runtime)
    val pg = getPgtool(spark, runtime, envir: String)
    implicit val solrConf = getSolrConf(runtime, envir: String)
    val omopTools = new OmopTools(pg, solrConf)

    val cohortDefinitionId = omopTools.getCohortDefinitionId(
      query.cohortDefinitionName,
      query.cohortDefinitionDescription,
      query.cohortDefinitionSyntax,
      query.ownerEntityId,
      query.delayInHours
    ) match {
      case Left(cohortDefinitionId) => cohortDefinitionId
      case Right(cohortDefinitionId) =>
        implicit val sparkImpl = spark
        implicit val solrSolver
          : (BasicResource) => io.frama.parisni.spark.query.Query =
          QuerySolver.fhirSolrGet
        val json = QueryParser.parse(query.cohortDefinitionSyntax)
        val cohort = QueryBuilder
          .getQuery(json)
          .df
          .dropDuplicates("patient")
          .withColumnRenamed("patient", "subject_id")
          .persist

        omopTools.loadCohort(cohortDefinitionId, cohort.count, cohort)
        cohort.unpersist()
        cohortDefinitionId
    }
    Array(Map("group.id" -> cohortDefinitionId))
  }

  def getPgtool(spark: SparkSession, runtime: JobEnvironment, envir: String) = {
    val pgHost =
      runtime.contextConfig.getString(
        s"passthrough.cohort360.${envir}.postgres.host")
    val pgPort =
      runtime.contextConfig.getString(
        s"passthrough.cohort360.${envir}.postgres.port")
    val pgDb =
      runtime.contextConfig.getString(
        s"passthrough.cohort360.${envir}.postgres.database")
    val pgSchema =
      runtime.contextConfig.getString(
        s"passthrough.cohort360.${envir}.postgres.schema")
    val pgUser =
      runtime.contextConfig.getString(
        s"passthrough.cohort360.${envir}.postgres.user")
    PGTool(
      spark,
      s"jdbc:postgresql://${pgHost}:${pgPort}/${pgDb}?user=${pgUser}&currentSchema=${pgSchema},public",
      "postgres-spark-job"
    )
  }

  def parse(str: String) = {
    val res = Json.parse(str)

    implicit val locationReads: Reads[Query] = (
      (JsPath \ "cohortDefinitionName").read[String] and
        (JsPath \ "cohortDefinitionDescription").readNullable[String] and
        (JsPath \ "cohortDefinitionSyntax").read[String] and
        (JsPath \ "ownerEntityId").read[Long] and
        (JsPath \ "delayInHours").read[Int]
    )(Query.apply _)

    res.validate[Query]
  }

  def getSolrConf(runtime: JobEnvironment, envir: String) = {
    val zkHost =
      runtime.contextConfig.getString(s"passthrough.cohort360.${envir}.solr.zk")
    val options = Map(
      "zkhost" -> zkHost,
      "commit_within" -> "100",
      "batch_size" -> "5000",
      "timezone_id" -> "Europe/Paris",
      "request_handler" -> "/export"
    )
    options
  }

  def getEnvir(runtime: JobEnvironment) = {
    runtime.contextConfig.getString("passthrough.cohort360.profile.active")
  }

  override def validate(
      sc: SparkSession,
      runtime: JobEnvironment,
      config: Config
  ): JobData Or Every[ValidationProblem] = {
    Try(config.getString("input.string"))
      .map(words => Good(words))
      .getOrElse(Bad(One(SingleProblem("No input.string param"))))
  }
}
